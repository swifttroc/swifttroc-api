import moogose from 'mongoose';
import { config } from 'dotenv';
config();
import 'colors';
import logger from './src/utils/logger';

const MONGO_URL: string = process.env.MONGO_URL!;

moogose.connect(MONGO_URL)
.then(() => {
    console.log('Connected to MongoDB'.green);
    logger.info('Connected to MongoDB');
}).catch((error) => {
    console.error('Error connecting to MongoDB: '.red);
    logger.error('Error connecting to MongoDB: ', error);
    return 1;
});






