import { Schema, model } from 'mongoose';

const exampleSchema: any = new Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, { timestamps: true });

export default model('Example', exampleSchema);
