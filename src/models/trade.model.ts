import { Schema, model, Types } from 'mongoose';

const tradeSchema: any = new Schema({
    proposer: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    recipient: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    proposerItems: [{
        type: Schema.Types.ObjectId,
        ref: 'Object',
        required: true
    }],
    recipientItems: [{
        type: Schema.Types.ObjectId,
        ref: 'Object',
        required: true
    }],
    status: {
        type: String,
        enum: ['pending', 'accepted', 'rejected'],
        default: 'pending'
    },
    recipientAcceptedTrade: {
        type: Boolean,
        default: false
    },
    proposerAcceptedTrade: {
        type: Boolean,
        default: false
    },
}, { timestamps: true });

export default model('Trade', tradeSchema);

// import { Schema, model, Types } from 'mongoose';

// const tradeSchema: any = new Schema({
//     tradeSender: {
//         type: Schema.Types.ObjectId,
//         ref: 'User',
//         required: true
//     },
//     tradeReceiver: {
//         type: Schema.Types.ObjectId,
//         ref: 'User',
//         required: true
//     },
//     senderObjects: [{
//         type: Schema.Types.ObjectId,
//         ref: 'Annonce',
//         required: true
//     }],
//     receiverObjects: [{
//         type: Schema.Types.ObjectId,
//         ref: 'Annonce',
//         required: true
//     }],
//     tradeStatus: {
//         type: String,
//         enum: ['pending', 'accepted', 'rejected'],
//         default: 'pending'
//     }
// }, { timestamps: true });

// export default model('Trade', tradeSchema);