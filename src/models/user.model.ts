import { Schema, model } from 'mongoose';

const userSchema: any = new Schema({
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    pseudo: {
        type: String,
        required: true,
        unique: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    verificationCode: {
        type: String, 
    },
    isPhoneVerified: {
        type: Boolean,
        required: false,
    },
    password: {
        type: String,
        required: true
    },
    localisation: {
        latitude: {
            //Float Number
            type: Schema.Types.Decimal128,
            required: false,
            default: 0
        },
        longitude: {
            type: Schema.Types.Decimal128,
            required: false,
            default: 0
        }
    },
    avatar: {
        type: String,
        required: false,
        default: 'https://via.placeholder.com/150'
    },
    trades: [{
        type: Schema.Types.ObjectId,
        ref: 'Trade'
    }],
    objects: [{
        type: Schema.Types.ObjectId,
        ref: 'Object'
    }],
    isPremium: {
        type: Boolean,
        default: false
    },
    isBanned: {
        type: Boolean,
        default: false
    },
}, { timestamps: true });

export default model('User', userSchema);

