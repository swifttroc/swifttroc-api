import { Schema, model } from 'mongoose';

const objectSchema: any = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    categories: {
        type: Array,
        required: true
    },
    images: {
        type: Array,
        required: true
    },
    publicationDate: {
        type: Date,
        default: Date.now
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    isTraded: {
        type: Boolean,
        default: false
    }

}, { timestamps: true });

export default model('Object', objectSchema);

