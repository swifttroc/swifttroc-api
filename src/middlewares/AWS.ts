//AWS middleware (upload images to S3)
import AWS from 'aws-sdk';
import fs from 'fs';
import dotenv from 'dotenv';
import { Request, Response, NextFunction } from 'express';

dotenv.config();

// Configuration AWS
const s3 = new AWS.S3({
  accessKeyId: process.env.accessKeyId,
  secretAccessKey: process.env.secretAccessKey,
  region: 'eu-west-3',
});

export const uploadFilesToS3 = async (req: any, res: any, next: NextFunction) => {
  console.log('uploadAWSMiddleware()'.yellow);

  console.log('req.files', req.files);
  console.log('req.body', req.body);

  if (!req.files || !Array.isArray(req.files)) {
      return res.status(400).json({ message: 'No files uploaded' });
  }

  try {
      const uploadedFiles = await Promise.all(req.files.map(async (file: Express.Multer.File) => {
          const filePath = file.path;
          const fileStream = fs.createReadStream(filePath);
          const fileName = `uploads/${Date.now()}_${file.originalname}`; // Unique file name

          const uploadParams = {
              Bucket: 'swifttrocbucket',
              Key: fileName,
              Body: fileStream,
          };

          // Upload file to S3
          const data = await s3.upload(uploadParams).promise();

          // Delete file from local storage after upload
          deleteFileFromStorage(filePath);

          return {
              s3Url: data.Location,
              cfUrl: `${process.env.cloudFrontUrl}/${fileName}`,
          };
      }));

      // Attach S3 URLs and CloudFront URLs to the request object
      req.s3Urls = uploadedFiles.map(file => file.s3Url);
      req.cfUrls = uploadedFiles.map(file => file.cfUrl);

      next();
  } catch (error) {
      console.error('Error uploading to S3:', error);
      return res.status(500).json({ message: 'Error uploading to S3' });
  }
};

export const uploadFileToS3 = async (req: any, res: any, next: NextFunction) => {
  console.log('uploadFileToS3()'.yellow);

  console.log('req.file', req.file);
  console.log('req.body', req.body);

  if (!req.file) {
      return res.status(400).json({ message: 'No file uploaded' });
  }

  try {
      const file = req.file;
      const filePath = file.path;
      const fileStream = fs.createReadStream(filePath);
      const fileName = `uploads/${Date.now()}_${file.originalname}`; // Unique file name

      const uploadParams = {
          Bucket: 'swifttrocbucket',
          Key: fileName,
          Body: fileStream,
      };

      // Upload file to S3
      const data = await s3.upload(uploadParams).promise();

      // Delete file from local storage after upload
      deleteFileFromStorage(filePath);

      // Attach S3 URLs and CloudFront URLs to the request object
      req.s3Url = data.Location;
      req.cfUrl = `${process.env.cloudFrontUrl}/${fileName}`;

      next();
  } catch (error) {
      console.error('Error uploading to S3:', error);
      return res.status(500).json({ message: 'Error uploading to S3' });
  }
};

const deleteFileFromStorage = (filePath: any) => {
  console.log('deleteFileFromStorage()'.yellow);
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(err);
      return;
    }
    //file removed
    console.log('File removed from storage'.green);
  });
};
