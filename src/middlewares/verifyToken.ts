import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';
import logger from '../utils/logger';

export const verifyToken = (req: any, res: Response, next: NextFunction) => {
    console.log('Verifying token...'.green);
    const token = req.header('auth-token');
    if (!token) {
        console.log('Access Denied'.red);
        logger.info('Access Denied');
        return res.status(401).json({ message: "Access Denied" });
    }

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET!);
        req.user = verified;
        console.log('Token verified'.green);
        next();
    } catch (error) {
        console.log('Invalid Token'.red);
        logger.info('Invalid Token');
        return res.status(400).json({ message: "Invalid Token" });
    }
}