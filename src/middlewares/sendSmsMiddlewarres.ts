import 'dotenv/config';
import twilio from 'twilio';
import { Request, Response, NextFunction } from 'express';
import { parsePhoneNumberFromString } from 'libphonenumber-js';

// Define the SMS middleware function
const sendSmsMiddleware = async (req: Request, res: Response, next: NextFunction): Promise<any> => {
    console.log('Sending SMS...'.magenta);

    const { phone, pseudo, verificationCode } = req.body;

    if (!phone) {
        return res.status(400).json({ message: "Phone number is required" });
    }


    // Load environment variables
    const accountSid = process.env.twilioAccountSID;
    const authToken = process.env.twilioAuthToken;
    const fromPhoneNumber = process.env.twilioPhoneNumber;
    
    try {
        const phoneNumber = parsePhoneNumberFromString(phone, 'FR');
        if (!phoneNumber || !phoneNumber.isValid()) {
            return next(new Error('Numéro de téléphone invalide'));
        }

        const formattedPhone = phoneNumber.number; // E.164 format
        console.log(`Formatted phone number: ${formattedPhone}`);

        // Create a Twilio client instance
        const client = twilio(accountSid, authToken);

        // Define the SMS message
        const messageBody = `Bonjour ${pseudo} et bienvenue sur SwiftTroc, votre code 
        de vérification est : ${verificationCode}
        `;
        // Send an SMS message
        await client.messages.create({
            body: messageBody,
            from: fromPhoneNumber,
            to: formattedPhone
        });
        console.log('[SMS_SEND_SUCCESS]'.green);
        next();
    } catch (error : any) {
        return next(error);
    }
};

export default sendSmsMiddleware;
