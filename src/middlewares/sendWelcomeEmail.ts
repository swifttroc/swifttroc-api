import nodemailer from 'nodemailer'

interface EmailData {
    userName: string;
    verificationCode?: string;
}

const sendWelcomeEmail = async (template: string, to: string, data: EmailData): Promise<void> => {
    const transporter = nodemailer.createTransport({
        host: process.env.smtpHost,
        port: 465,
        secure: true,
        auth: {
            user: process.env.userEmail,
            pass: process.env.userPass,
        },
    });

    const mailOptions = {
        from: '<webserviceynov@gmail.com>',
        to,
        subject: "Vérification téléphone - SwiftTroc",
        text: `Hello ${data.userName}, Bienvenue sur SwiftTroc`,
        html: `<b>Hello ${data.userName}</b>, Bienvenue sur SwiftTroc, 
        avant de pouvoir utiliser nos services, merci de vérifier votre numéro de téléphone 
        avec le code suivant : 
        ${data.verificationCode}
        `,
    };

    await transporter.sendMail(mailOptions);
};

export default sendWelcomeEmail;
