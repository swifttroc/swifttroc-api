// src/middlewares/sendEmailMiddleware.ts
import { Request, Response, NextFunction } from 'express';
import sendWelcomeEmail from './sendWelcomeEmail';

const sendEmailMiddleware = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { email, name, verificationCode } = req.body; 
    try {
        await sendWelcomeEmail('registrationConfirmation', email, { userName: name, verificationCode: verificationCode });
        next(); // Proceed to the next middleware or route handler
    } catch (error) {
        console.error("Error sending email:", error);
        res.status(500).json({ message: "Registration succeeded, but sending the email failed." });
    }
    next()
};

export default sendEmailMiddleware;
