import { Router } from 'express';
import { exampleController } from '../controllers/example.controller';

const router = Router();

router.get('/', exampleController.get);
router.post('/', exampleController.post);


export default router;
