import { Router } from 'express';

import { userController } from '../controllers/user.controller';
import { verifyToken } from '../middlewares/verifyToken';
// import sendEmailMiddleware from '../middlewares/sendEmailMiddlewares';
import multer from 'multer';
import fs from 'fs';
import { uploadFileToS3 } from '../middlewares/AWS';

const createUploadsDirectory = () => {
    const directory = './src/uploads/';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory, { recursive: true });
    }
};

createUploadsDirectory();
  
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './src/uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  });

const upload = multer({ storage: storage })

const router = Router();
router.get('/', userController.get);
router.get('/countNumberOfUsers', userController.getNumberOfUsers);
router.get('/me', verifyToken, userController.getMe);
router.get('/:id', userController.getUserByID);
router.post('/register', userController.register);
router.post('/login', userController.login);
router.post('/start-registration', userController.startRegistration);
router.post('/verify-phone', userController.verifyPhoneNumber);
router.put('/updateLocation', verifyToken, userController.updateLocation);
router.put('/noavatar', verifyToken, userController.updateUserNoAvatar);
router.put('/updateUser',
    verifyToken,
    upload.single('avatar'),
    (req, res, next) => {
      uploadFileToS3(req, res, next);
    },
    userController.updateUser
);



export default router;
