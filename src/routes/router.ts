// router 
import { Router } from 'express';

import ExampleRoutes from './example.routes';
import UserRoutes from './user.routes';
import ObjectRoutes from './object.routes';
import TradeRoutes from './trade.routes';

const router = Router();

router.use('/example', ExampleRoutes);
router.use('/user', UserRoutes);
router.use('/object', ObjectRoutes);
router.use('/trade', TradeRoutes);


export default router;