import { Router } from 'express';
import { tradeController } from '../controllers/trade.controller';
import { verifyToken } from '../middlewares/verifyToken';

const router = Router();

router.get('/', verifyToken, tradeController.GET);
router.post('/', verifyToken, tradeController.createTrade);
router.put('/', verifyToken, tradeController.updateTradeStatus);
router.put('/:tradeId/boolean', verifyToken, tradeController.updateTradeBoolean);



export default router;
