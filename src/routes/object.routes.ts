
import { Router } from 'express';
import { objectController } from '../controllers/object.controller';
import { verifyToken } from '../middlewares/verifyToken';
import { uploadFilesToS3 } from '../middlewares/AWS';

import fs from 'fs';
import multer from 'multer';

const router = Router();

const createUploadsDirectory = () => {
    const directory = './src/uploads/';
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory, { recursive: true });
    }
};
  
  // Create the uploads directory before setting up multer.diskStorage
  createUploadsDirectory();
  
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './src/uploads/');
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
  });

const upload = multer({ storage: storage })

router.get('/', objectController.get);
router.get('/user-specific/:id', objectController.getUserObjects);
router.get('/me', verifyToken, objectController.myObjects);
router.get('/withowner', objectController.getwithOwner);
// router.get('/user/:id', verifyToken, objectController.myObjects);
router.get('/filtered', verifyToken, objectController.getFiltered1);
router.delete('/:id', objectController.deleteById);
router.get('/countNumberOfObjects',objectController.getNumberOfObjects);
router.post('/',
    verifyToken,
    upload.array('images', 5),
    (req, res, next) => {
        uploadFilesToS3(req, res, next);
    },
    objectController.post
);



export default router;

