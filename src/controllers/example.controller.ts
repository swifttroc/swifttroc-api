import { Request, Response } from 'express';
import exampleModel from '../models/example.model';

export const exampleController = {
    get: (req: Request, res: Response) => {
        res.send('Example !');
    },
    post: (req: Request, res: Response) => {
        const body = {
            name: "John Doe",
            description: "This is an example",
        }

        const example = new exampleModel(body);
        example.save();

        return res.status(200).json(example);  

    },
};