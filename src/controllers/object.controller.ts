import { Request, Response } from 'express';
import Object from '../models/object.model';
import User from '../models/user.model';
import logger from '../utils/logger';

export const objectController = {
    // Récupération de tous les objets
    get: async (req: Request, res: Response) => {
        console.log('getObjects'.cyan);
        try {
            const objects = await Object.find();
            return res.status(200).json(objects);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    getwithOwner: async (req: Request, res: Response) => {
        console.log('getObjectswithOwner'.cyan);
        try {
            // Utilise populate pour récupérer les informations du propriétaire de l'objet
            const objects = await Object.find().populate('owner', 'name lastName');
            return res.status(200).json(objects);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    
    // Récupération des objets avec filtres
    getFiltered: async (req: Request, res: Response) => {
        console.log('getFilteredObjects'.cyan);
        const { title, description, category } = req.query;
    
        let query: any = {};
    
        if (title) {
            query.title = { $regex: `.*${title}.*`, $options: 'i' }; // Recherche insensible à la casse et match n'importe où dans la chaîne
        }
    
        if (description) {
            query.description = { $regex: `.*${description}.*`, $options: 'i' }; // Recherche insensible à la casse et match n'importe où dans la chaîne
        }
    
        if (category) {
            const categoriesArray = (category as string).split(','); // Divise la chaîne de catégories en un tableau
            query.categories = { $in: categoriesArray }; // Utilise $in pour vérifier si l'une des catégories correspond
        }
    
        try {
            const objects = await Object.find(query);
            return res.status(200).json(objects);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    
    // Nouvelle méthode : Récupération des objets filtrés par localisation et rayon
    getFiltered1: async (req: any, res: Response) => {
        console.log('getFilteredObjectsByLocation'.cyan);
        const { title, description, category, localisation, radius } = req.query;
        const userID = req.user._id; // Get the current user's ID
    
        let query: any = {};
    
        // Filtrer par titre et description
        if (title) {
            query.title = { $regex: `.*${title}.*`, $options: 'i' }; // Recherche insensible à la casse
        }
    
        if (description) {
            query.description = { $regex: `.*${description}.*`, $options: 'i' }; // Recherche insensible à la casse
        }
    
        if (category) {
            const categoriesArray = (category as string).split(',');
            query.categories = { $in: categoriesArray };
        }
    
        // Always exclude objects where the owner is the current user
        query.owner = { $ne: userID };
    
        if (localisation && radius) {
            const [latitude, longitude] = (localisation as string).split(',').map(parseFloat);
            const maxDistanceInMeters = parseFloat(radius as string) * 1000;
    
            try {
                // Trouver les utilisateurs dans le rayon spécifié en convertissant latitude et longitude de Decimal128 à Double
                const users = await User.find({
                    'localisation.latitude': { $exists: true }, 
                    'localisation.longitude': { $exists: true },
                    $expr: {
                        $lt: [
                            {
                                $sqrt: {
                                    $add: [
                                        { $pow: [{ $subtract: [latitude, { $toDouble: "$localisation.latitude" }] }, 2] },
                                        { $pow: [{ $subtract: [longitude, { $toDouble: "$localisation.longitude" }] }, 2] }
                                    ]
                                }
                            },
                            maxDistanceInMeters / 111320 // Approximation de la distance en degrés
                        ]
                    }
                }).select('_id');
    
                // Only include objects where owner is in the list of users from the filtered location
                query.owner = { $in: users.map(user => user._id), $ne: userID };
    
                const objects = await Object.find(query);
                return res.status(200).json(objects);
            } catch (error) {
                console.log('Error fetching filtered objects:', error);
                return res.status(500).json({ message: error });
            }
        } else {
            try {
                // Find objects that match the filters (excluding the current user's objects)
                const objects = await Object.find(query);
                return res.status(200).json(objects);
            } catch (error) {
                console.log('Error fetching filtered objects:', error);
                return res.status(500).json({ message: error });
            }
        }
    },

    // Création d'un objet
    post: (req: any, res: Response) => {
        console.log('postObject'.cyan);
        const { title, description, categories } = req.body;

        const body = {
            title,
            description: description || "Aucune description",
            categories,
            images: req.s3Urls,
            owner: req.user._id
        }

        try {
            const object = new Object(body);
            object.save();

            // Ajouter l'objet à la liste des objets de l'utilisateur
            User.findByIdAndUpdate(req.user._id, { $push: { objects: object._id } }).exec();

            return res.status(200).json(object);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },

    deleteById: async (req: any, res: Response) => {
        console.log('deleteObjectById'.cyan);
        try {
            const objectId = req.params.id; // ID de l'objet à supprimer
    
            // Récupérer l'objet pour obtenir son propriétaire
            const object = await Object.findById(objectId);
            if (!object) {
                return res.status(404).json({ error: "Objet non trouvé." });
            }
    
            const ownerId = object.owner; // Stocker temporairement l'ID du propriétaire de l'objet
            console.log('Owner ID:', ownerId);
            // Supprimer l'objet
            await Object.deleteOne({ _id: objectId });
    
            // Retirer l'objet du tableau 'objects' du propriétaire (owner)
            await User.updateOne(
                { _id: ownerId },
                { $pull: { objects: objectId } } // Retirer l'ID de l'objet du tableau 'objects'
            );
    
            console.log(`Objet ${objectId} supprimé et retiré de l'utilisateur ${ownerId}.`);
            return res.status(200).json({ message: "Objet supprimé avec succès." });
        } catch (error) {
            console.error(error);
            return res.status(500).json({ error: 'Erreur lors de la suppression de l\'objet.' });
        }
    },
    
    // Récupération des objets de l'utilisateur connecté
    myObjects: async (req: any, res: Response) => {
        console.log('getMyObjects'.cyan);
        try {
            const objects = await Object.find({ owner: req.user._id });
            return res.status(200).json(objects);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    getNumberOfObjects: async (req: any, res: Response) => {
        console.log('countNumberOfObjects()'.cyan);
        try {
            // Nombre total d'objets
            const numberOfObjects = await Object.countDocuments();
    
            // Obtenir la date de début de la journée actuelle
            const startOfDay = new Date();
            startOfDay.setHours(0, 0, 0, 0); // Mettre les heures à 00:00:00
    
            // Obtenir la date de fin de la journée actuelle
            const endOfDay = new Date();
            endOfDay.setHours(23, 59, 59, 999); // Mettre les heures à 23:59:59
    
            // Compter les objets créés aujourd'hui
            const numberOfObjectsToday = await Object.countDocuments({
                createdAt: {
                    $gte: startOfDay, // Plus grand ou égal au début de la journée
                    $lt: endOfDay // Moins que la fin de la journée
                }
            });
    
            console.log('Total objects count: ', numberOfObjects);
            console.log('Objects created today: ', numberOfObjectsToday);
    
            // Envoyer les deux nombres dans la réponse
            res.json({ numberOfObjects, numberOfObjectsToday });
        } catch (error) {
            res.status(500).json({ error: 'Internal Server Error' });
        }
    },
    
    // Récupération des objets d'un utilisateur spécifique
    getUserObjects: async (req: any, res: Response) => {
        console.log(`getUserObjects(${req.params.id})`.cyan);
        const { id } = req.params;
        try {
            const objects = await Object.find({ owner: id });
            return res.status(200).json(objects);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    }
};
