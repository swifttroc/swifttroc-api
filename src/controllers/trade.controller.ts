import { Request, Response } from 'express';
import Trade from '../models/trade.model';
import ObjectModel from '../models/object.model';
import nodemailer from 'nodemailer';
import PDFDocument from 'pdfkit'; //ne pas oublier `npm i --save-dev @types/pdfkit`
import User from '../models/user.model';
import axios from 'axios';

//pas possible de mettre un url d'image dans un pdfkit, il faut donc fetch l'image et la convertir en buffer
const fetchImageBuffer = async (url: string): Promise<Buffer | null> => {
    try {
        const response = await axios.get(url, { responseType: 'arraybuffer' });
        return Buffer.from(response.data, 'binary');
    } catch (error) {
        console.error(`Failed to fetch image from ${url}:`, error);
        return null;
    }
}


const generatePDF = async (trade: any, proposer: any, recipient: any) => {
    
const doc = new PDFDocument({ margin: 50 });

// Header section
doc.fontSize(24).text('SWIFTTROC', { align: 'center' });
doc.moveDown(1.5); // Add more space after header

// Trade details section
doc.fontSize(16).text(`Détails du Troc`, { underline: true, align: 'left' });
doc.fontSize(12).text(`ID du troc: ${trade._id}`, { align: 'left' });
doc.text(`Proposant: ${proposer.name} ${proposer.lastName}`, { align: 'left' });
doc.text(`Receveur: ${recipient.name} ${recipient.lastName}`, { align: 'left' });
doc.text(`Statut: ${trade.status}`, { align: 'left' });
doc.moveDown(1.5);

const proposerItems = await ObjectModel.find({ _id: { $in: trade.proposerItems } });
const recipientItems = await ObjectModel.find({ _id: { $in: trade.recipientItems } });

doc.fontSize(14).text(`Objets proposés par ${proposer.name}:`, { underline: true });
doc.moveDown(0.5);

for (const item of proposerItems) {
    doc.fontSize(12).text(`- ${item.title}`);
    doc.moveDown(0.5);
    if (item.images.length > 0) {
        const imageBuffer = await fetchImageBuffer(item.images[0]);
        if (imageBuffer) {
            doc.image(imageBuffer, { fit: [150, 150], align: 'center' });
            doc.addPage();
            doc.moveDown(1);
        } else {
            doc.text('(Image non disponible)');
        }
    }
    doc.moveDown(1);
}

// Display recipient items with images
doc.moveDown(1.5);
doc.fontSize(14).text(`Objets reçus par ${recipient.name}:`, { underline: true });
doc.moveDown(0.5);

for (const item of recipientItems) {
    doc.fontSize(12).text(`- ${item.title}`);
    doc.moveDown(0.5);
    if (item.images.length > 0) {
        const imageBuffer = await fetchImageBuffer(item.images[0]);
        if (imageBuffer) {
            doc.image(imageBuffer, { fit: [150, 150], align: 'center' });
            doc.addPage();

            doc.moveDown(1);
        } else {
            doc.text('(Image non disponible)');
        }
    }
    doc.moveDown(1);
}

doc.end();
return doc;

};

const sendTradePDF = async (proposer: any, recipient: any, pdfBuffer: any) => {
    const transporter = nodemailer.createTransport({
        host: process.env.smtpHost,
        port: 465,
        secure: true,
        auth: {
            user: process.env.userEmail,
            pass: process.env.userPass,
        },
    });

    const mailOptions = {
        from: '<webserviceynov@gmail.com>',
        to: [proposer.email, recipient.email], //  to: [proposer.email, recipient.email],  to: '<koceila.arhab@gmail.com>'
        subject: "Vérification téléphone - SwiftTroc",
        text: `Bonjour, ci-joint, les détails de votre troc.`,
        html: ``,
        attachments: [
            {
                filename: 'details_du_troc.pdf',
                content: pdfBuffer, // Le PDF en pièce jointe
                contentType: 'application/pdf',
            }
        ]
    };
      console.log('pdfBuffer tentative d envoi du mail avec le pdf');
    return transporter.sendMail(mailOptions);
};
export const tradeController = {
    
    // Récupération de tous les trocs
    GET: async (req: any, res: Response) => {
        try {
            const id = req.user._id;
            //get user trade where he is the recipient
            const trades = await Trade.find()
                .populate('proposer')
                .populate('recipient')
                .populate('proposerItems')
                .populate('recipientItems')
                .or([{ proposer: id }, { recipient: id }])
                console.log('trades', trades);
                
            return res.status(200).json(trades);
        } catch (error) {
            return res.status(500).json({ message: 'Une erreur est survenue lors de la récupération des trocs.' });
        }
    },

    
    createTrade: async (req: any, res: Response) => {
        console.log('createTrade'.cyan);
        const { recipient, proposerItems, recipientItems } = req.body;
        console.log('body', req.body);

        const proposer = req.user._id;
        if (proposerItems.length === 0 || recipientItems.length === 0) {
            return res.status(400).json({ message: 'Un échange doit contenir au moins un objet pour chaque utilisateur.' });
        }
    
        try {
            // Security check: Ensure a user cannot send a trade proposition to himself
            if (proposer === recipient) {
                return res.status(400).json({ message: 'Vous ne pouvez pas envoyer un troc à vous-même.' });
            }
    
            const trade = new Trade({
                proposer: proposer,
                recipient: recipient,
                proposerItems: proposerItems,
                recipientItems: recipientItems
            });
    
            await trade.save();
            //TODO: Send SMS function() Kouci
    
            return res.status(201).json(trade);
        } catch (error) {
            return res.status(500).json({ message: 'Une erreur est survenue lors de la création du troc.' });
        }
    },
    

    // Mise à jour du statut d'un troc (accepter ou refuser un troc)
    updateTradeStatus: async (req: Request, res: Response) => {
        const { tradeId, status } = req.body;
        console.log(`updateTradeStatus(${tradeId},${status})`.cyan);

        try {
            const trade = await Trade.findById(tradeId);

            if (!trade) {
                return res.status(404).json({ message: "Désolé, ce troc n'existe pas." });
            }

            trade.status = status;
            await trade.save();

            // send a pdf to both users with the trade details


            return res.status(200).json(trade);
        } catch (error) {
            console.log('error', error);
            return res.status(500).json({ message:'Erreur lors de la mise à jour du statut du troc.' });
        }
    },
    // Mise à jour d'un champ booléen d'un troc (proposerAcceptedTrade ou recipientAcceptedTrade)

    // Controller function to handle boolean updates
updateTradeBoolean: async (req: Request, res: Response) => {
    const { field, value } = req.body;
    const { tradeId } = req.params;

    try {
        const updateField = { [field]: value };

        const trade = await Trade.findByIdAndUpdate(
            tradeId,
            { $set: updateField }, // Using $set to dynamically update the field
            { new: true }
        );

        if (!trade) {
            return res.status(404).json({ message: "Trade not found" });
        }

        // Fetch proposer and recipient details
        const proposer = await User.findById(trade.proposer);
        const recipient = await User.findById(trade.recipient);

        if (!proposer || !recipient) {
            return res.status(404).json({ message: "Impossible de trouver les utilisateurs." });
        }

        //s'assurer que les deux utilisateurs ont accepté le troc avant d'envoyer le pdf
        if (!trade.proposerAcceptedTrade || !trade.recipientAcceptedTrade) {
            return res.status(200).json(('Les deux utilisateurs doivent accepter le troc pour recevoir le PDF.'));
        }

        // Generate the PDF
        const pdfDoc = await generatePDF(trade, proposer, recipient);

        let buffers : any = [];
        pdfDoc.on('data', buffers.push.bind(buffers));
        pdfDoc.on('end', async () => {
            const pdfBuffer = Buffer.concat(buffers);

            // Send the PDF via email
            await sendTradePDF(proposer, recipient, pdfBuffer);
        });

        return res.status(200).json(trade);
    } catch (error) {
        console.error('Error updating trade:', error);
        return res.status(500).json({ message: "Server error" });
    }
},

    

};

