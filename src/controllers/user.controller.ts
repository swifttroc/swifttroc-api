import { Request, Response, NextFunction } from 'express';
import User from '../models/user.model';

import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import logger from '../utils/logger';
import sendSmsMiddleware from '../middlewares/sendSmsMiddlewarres';
import parsePhoneNumberFromString from 'libphonenumber-js';
import nodemailer from 'nodemailer';


export const userController = {

    get: async (req: Request, res: Response) => {
        try {
            const users = await User.find();
            return res.status(200).json(users);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },

    getMe: async (req: any, res: Response) => {
        console.log('Getting me...'.magenta);
        
        try {
            const user = await User.findById(req.user._id);
            return res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },

    getUserByID: async (req: Request, res: Response) => {
        console.log(`getUserByID(${req.params.id})`.magenta);
        try {
            const userId = req.params.id;
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).json({ message: "Utilisateur non trouvé" });
            }
            return res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error});
        }
    },
  
    getNumberOfUsers: async (req: Request, res: Response) => {
        console.log('getNumberOfUsers()'.cyan)
        try {
            // Nombre total d'utilisateurs
            const numberOfUsers = await User.countDocuments();
            
            // Obtenir la date de début de la journée actuelle
            const startOfDay = new Date();
            startOfDay.setHours(0, 0, 0, 0); // Met à 00:00:00
            
            // Obtenir la date de fin de la journée actuelle
            const endOfDay = new Date();
            endOfDay.setHours(23, 59, 59, 999); // Met à 23:59:59
    
            // Compter les utilisateurs créés aujourd'hui
            const numberOfUsersToday = await User.countDocuments({
                createdAt: {
                    $gte: startOfDay, // Plus grand ou égal au début de la journée
                    $lt: endOfDay // Plus petit que la fin de la journée
                }
            });
    
            console.log('Total users count: ', numberOfUsers);
            console.log('Users created today: ', numberOfUsersToday);
    
            // Envoyer les deux nombres dans la réponse
            res.json({ numberOfUsers, numberOfUsersToday });
        } catch (error) {
            console.log('Error:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        }
    },
    
    updateUser: async (req: any, res: Response) => {
        console.log('updateUser()'.cyan)
        try {
            const userId = req.user._id;
            const { name, lastName, pseudo , isBanned, isPremium} = req.body;
    
            // Trouver l'utilisateur par son ID
            const user = await User.findById(userId);
            if (!user) {
                return res.status(404).json({ message: "Utilisateur non trouvé" });
            }
    
            // Vérifier si l'email ou le pseudo sont déjà utilisés (si ces champs sont fournis)
            if (pseudo && pseudo !== user.pseudo) {
                const pseudoExist = await User.findOne({ pseudo });
                if (pseudoExist) {
                    return res.status(400).json({ message: "Ce pseudo est déjà utilisé" });
                }
            }
    
            // Mettre à jour les champs
            if (name) user.name = name;
            if (lastName) user.lastName = lastName;
            if (pseudo) user.pseudo = pseudo;
            if (isBanned !== undefined) user.isBanned = isBanned;
            if (isPremium !== undefined) user.isPremium = isPremium;
            // Vérifier si une image a été téléchargée sur S3 et mettre à jour l'avatar
            if (req.s3Url) {
                user.avatar = req.s3Url; // Met à jour l'avatar avec l'URL renvoyée par S3
            }
    
            // Sauvegarder les modifications
            await user.save();
    
            return res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    updateUserNoAvatar: async (req: any, res: Response) => {
        console.log('updateUserNoAvatar()'.cyan)
        try {
            const userId = req.user._id;
            const { name, lastName, pseudo , isBanned, isPremium} = req.body;
            console.log('user id request',userId);
            //console log name lastName pseudio is banned is premium from req.body
            console.log('name ',name);
            console.log('lastName ',lastName);
            console.log('pseudo ',pseudo);
            console.log('isBanned ',isBanned);
            console.log('isPremium ',isPremium);
            // Trouver l'utilisateur par son ID
            const user = await User.findById(userId);
            console.log('user id trouvé',user);
            if (!user) {
                return res.status(404).json({ message: "Utilisateur non trouvé" });
            }
    
            // Vérifier si l'email ou le pseudo sont déjà utilisés (si ces champs sont fournis)
            if (pseudo && pseudo !== user.pseudo) {
                const pseudoExist = await User.findOne({ pseudo });
                if (pseudoExist) {
                    return res.status(400).json({ message: "Ce pseudo est déjà utilisé" });
                }
            }

            // Mettre à jour les champs
            if (name) user.name = name;
            if (lastName) user.lastName = lastName;
            if (pseudo) user.pseudo = pseudo;
            if (isBanned !== undefined) user.isBanned = isBanned;
            if (isPremium !== undefined) user.isPremium = isPremium;
            // Sauvegarder les modifications
            await user.save();
            console.log('user id après enregistrement',user);
            return res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    },
    
    register: async (req: Request, res: Response,  next: NextFunction) => {
        console.log('Registering...'.magenta);
        
        const { name, lastName, pseudo, email, phone, password } = req.body;
        //check if all fields are filled

        if(!name) return res.status(400).json({ message: "Le prénom est requis" });
        if(!lastName) return res.status(400).json({ message: "Le nom est requis" });
        if(!pseudo) return res.status(400).json({ message: "Le pseudo est requis" });
        if(!email) return res.status(400).json({ message: "L'email est requis" });
        if(!phone) return res.status(400).json({ message: "Le numéro de téléphone est requis" });
        if(!password) return res.status(400).json({ message: "Le mot de passe est requis" });

        console.log(req.body);
        try {
            //hash password
            const hashedPassword = bcrypt.hashSync(password, 10);

            // Générer le code pour vérifier le numéro de téléphone
            const verificationCode = Math.floor(1000 + Math.random() * 9000).toString();

            //formated phone number
            const phoneNumber = parsePhoneNumberFromString(phone, 'FR');
            const formattedPhone = phoneNumber?.number;

            const body = {
                name,
                lastName,
                pseudo,
                email: email.toLowerCase(),
                formattedPhone,
                password: hashedPassword,
                verificationCode,
                isPhoneVerified: false // Initially false
            }
console.log('VERIFICATION CODE',verificationCode);
            //if email or pseudo or phone already exist
            const phoneExist = await User.findOne({ formattedPhone });
            const emailExist = await User.findOne({ email });
            const pseudoExist = await User.findOne({ pseudo });

            if (phoneExist) return res.status(400).json({ message: "Ce numéro de téléphone est déjà utilisé" });
            if (emailExist) return res.status(400).json({ message: "Cet email est déjà utilisé" });
            if (pseudoExist) return res.status(400).json({ message: "Ce pseudo est déjà utilisé" });
            

            const user = new User(body);
            // Store the user in the request body to be used by the middleware
            //toutes les variables nouvellement générées sont passées aux middlewares comme ceci :
            req.body.user = user;
            req.body.verificationCode = verificationCode;
            await sendSmsMiddleware(req, res, () => {});
            user.save();





            return res.status(200).json(user); 
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }

    },

    login : async (req: Request, res: Response) => {
        const { email, phone, password } = req.body;
        console.log(req.body);

        //remove phone spaces
        req.body.phone = req.body.phone.replace(/\s/g, '');
        const phoneNumber = parsePhoneNumberFromString(phone, 'FR');
        if (!email && (!phoneNumber || !phoneNumber?.isValid())) {
            console.log('[PHONE_ERROR]'.red, 'Invalid phone number');
            return res.status(400).json({ message: "Numéro de téléphone ou email invalide" });
        }

        const formattedPhone = phoneNumber?.number;
        console.log(`Formatted phone number: ${formattedPhone}`)
        
        //login with phone password or email password
        try {
            const user = await User.findOne({ $or: [ { email: email?.toLowerCase() }, { phone: formattedPhone } ] } );
            if (!user) return res.status(400).json({ message: "Email ou téléphone incorrect" });

            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) return res.status(400).json({ message: "Mot de passe incorrect" });

            //create and assign a token
            const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET!, { expiresIn: '4h' });
            logger.info(`User ${user._id} logged in`);
            console.log('le token de l\'utilisateur est :', token);
            return res.header('auth-token', token).json({ token, user });
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error});
        }
    },
    
    verifyPhoneNumber : async (req: any, res: any) => {
        console.log('Verifying phone...'.magenta);
        const { verificationCode } = req.body;

        console.log(req.body);
        console.log(req.session.tempUser);
       
        const tempUser = req.session.tempUser;

        if (!tempUser?.phone || tempUser.verificationCode !== verificationCode) {
            console.log('[VERIFICATION_ERROR]'.red, 'Invalid verification code or phone number');
            return res.status(400).json({ message: "Code de vérification invalide" });
        }

        const phoneNumber = parsePhoneNumberFromString(tempUser.phone, 'FR');
        const formattedPhone = phoneNumber?.number;

        // Mark the phone as verified
        tempUser.isPhoneVerified = true;

        // Save the user to the database
        const user = new User({
            name: tempUser.name,
            lastName: tempUser.lastName,
            pseudo: tempUser.pseudo,
            email: tempUser.email,
            phone: formattedPhone,
            password: tempUser.password,
            isPhoneVerified: tempUser.isPhoneVerified,
        });

        try {
            await user.save();
            const transporter = nodemailer.createTransport({
                host: process.env.smtpHost,
                port: 465,
                secure: true,
                auth: {
                    user: process.env.userEmail,
                    pass: process.env.userPass,
                },
            });
            
            const mailOptions = {
                from: '<webserviceynov@gmail.com>',
                to: `${tempUser.email}`,
                subject: "Vérification téléphone - SwiftTroc",
                text: `Hello ${tempUser.pseudo}, Bienvenue sur SwiftTroc ! Happy trading !`,
                html: `Hello ${tempUser.pseudo}, Bienvenue sur SwiftTroc ! Happy trading !`,
            };
            
            try {
                await transporter.sendMail(mailOptions);
                console.log("Email sent successfully");
            } catch (error) {
                console.log("Error sending email: ", error);
                return res.status(500).json({ message: "Failed to send email" });
            }

            // Clear the session or cache
            req.session.tempUser = null;

            console.log('[VERIFICATION_SUCCESS]'.green);
            res.status(200).json({ message: "Téléphone vérifié avec succès" });
        } catch (error: any) {
            console.log('[VERIFICATION_ERROR]'.red, `"${error}"`);
            res.status(500).json({ message: error?.message });
        }
    },

    startRegistration : async (req: any, res: any) => {
        console.log('Start registration...'.magenta);
        const { name, lastName, pseudo, email, phone, password } = req.body;

        if (!name || !lastName || !pseudo || !email || !phone || !password) {
            return res.status(400).json({ message: "Tous les champs sont requis" });
        }

        try {
            const hashedPassword = bcrypt.hashSync(password, 10);
            const verificationCode = Math.floor(1000 + Math.random() * 9000).toString();

            // Temporarily store the registration data, e.g., in a session or cache
            const tempUser = {
                name,
                lastName,
                pseudo,
                email,
                phone,
                password: hashedPassword,
                verificationCode,
                isPhoneVerified: false,
            };

            // Save tempUser to session
            req.session.tempUser = tempUser;
            req.body.verificationCode = verificationCode;

            // Send SMS
            await sendSmsMiddleware(req, res, (err?: any) => {
                if (err) {
                    console.log('[SMS_ERROR]'.red, `"${err}"` );
                    throw err;
                }
            });

            return res.status(200).json({ message: "Code de vérification envoyé" });
        } catch (error: any) {
            return res.status(500).json({ message: error?.message });
        }
    },

    updateLocation: async (req: any, res: Response) => {
        console.log('Updating location...'.magenta);
        const userId = req.user._id;
        const { latitude, longitude } = req.body;

        try {
            const user = await User.findById(userId);

            if (!user) return res.status(404).json({ message: "Utilisateur non trouvé" });

            user.localisation = { latitude, longitude };
            await user.save();

            return res.status(200).json(user);
        } catch (error) {
            logger.error(error);
            return res.status(500).json({ message: error });
        }
    }
            
};