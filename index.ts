import express, { Request, Response } from 'express';
import session from 'express-session';
import router from './src/routes/router';
import cors from 'cors';
import dotenv from 'dotenv';
dotenv.config();

import 'colors';
import logger from './src/utils/logger';
import './config'

const app = express();
const port = process.env.PORT || 4000;


app.use(cors(
    {
        origin: '*',
        credentials: true
    }
));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret: 'your-secret-key', // Replace with a strong secret key
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false } // Set to true if using HTTPS
}));

app.use((req: Request, res: Response, next) => {
    logger.info(`${req.method} ${req.originalUrl}`);
    next();
});

app.use('/api', router);

app.get('/', (req: Request, res: Response) => {
    res.send('SwiftTroc API is running !');
});


app.listen(port, () => {
console.log(`Server running at http://localhost:${port}`.magenta);
});